import { Component, Input } from '@angular/core';
import { DataLocalService } from '../../services/data-local.service';
import { Registro } from '../models/registro.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  @Input() qrs: Registro[] = [];

  constructor(public dataLocal: DataLocalService) {
    this.qrs = dataLocal.guardados;
  }

  abrirRegistro(registro) {
    this.dataLocal.abrirRegistro(registro);
  }

  enviarCorreo() {
    this.dataLocal.enviarCorreo();
  }

}
